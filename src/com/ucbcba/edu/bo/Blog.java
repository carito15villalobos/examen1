package com.ucbcba.edu.bo;

import java.util.LinkedList;
import java.util.List;

public class Blog extends SitioWeb {
    private Usuario creador;
    private List<Articulo> articulos;

    public Blog(String nombre, Usuario creador) {
        this.nombre=nombre;
        this.creador= creador;
        articulos = new LinkedList<>();
    }
    public void anadirArticulo(Articulo articulo){
        articulos.add(articulo);
    }
    public void showBlog(){
        System.out.println("Blog:  "+nombre);
        System.out.println("    "+getUrl());
        for(int i=0;i<numeroArticulos();i++){
            articulos.get(i).showArticulo();
        }

    }
    int numeroArticulos(){
        return articulos.size();
    }

}
