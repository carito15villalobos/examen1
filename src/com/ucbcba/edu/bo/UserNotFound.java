package com.ucbcba.edu.bo;

public class UserNotFound extends Exception {

    public UserNotFound(String message) {
        super("usuario no encontrado : "+message);
    }


}
