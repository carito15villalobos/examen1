package com.ucbcba.edu.bo;

public class Usuario {
    private String nombre;
    private String apellido;

    public Usuario(String nombre, String apellido){
        this.nombre=nombre;
        this.apellido = apellido;
    }

    public Usuario() {
    }

    public void mostrarNombre(){
        System.out.println(nombre +" "+ apellido);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
